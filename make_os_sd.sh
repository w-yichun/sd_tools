#!/bin/bash
# ************************Variable*********************************************
ScriptPath="$( cd "$(dirname "$0")" ; pwd -P )""/"
DEV_NAME=$1
MAKECONF="mksd.conf"
#********************** UPDATE TO FIX ISSUE REGARDING /dev/mmc* MOUNTED SD CARD **********

if [[ $DEV_NAME == /dev/mmc* ]]
then
    echo "Partion naming with p-prefix"	
    p1="p1"
    p2="p2"
    p3="p3"
    p4="p4"
else
    echo "Partition naming without p-prefix"	
    p1="1"
    p2="2"
    p3="3"
    p4="4"
fi

#####################################################################################


ISO_FILE_DIR=$2
ISO_FILE=$3

DRIVER_PACKAGE=$(ls Ascend310-driver-*.tar.gz)

NETWORK_CARD_DEFAULT_IP=$4
USB_CARD_DEFAULT_IP=$5

MAKE_OS_RESULT=$6

LogPath=${ScriptPath}"sd_card_making_log/"
TMPDIR_SD_MOUNT=${LogPath}"sd_mount_dir"
TMPDIR_SD2_MOUNT=${LogPath}"sd_mount_dir2"
TMPDIR_SD3_MOUNT=${LogPath}"sd_mount_dir3"
TMPDIR_SD4_MOUNT=${LogPath}"sd_mount_dir4"
TMPDIR_DATE=${LogPath}"no_touch_make_sd_dir"

USER_NAME="HwHiAiUser"
USER_PWD="HwHiAiUser:\$6\$klSpdQ1K\$4Gm/7HxehX.YSum4Wf3IDFZ3v5L.clybUpGNGaw9zAh3rqzqB4mWbxvSTFcvhbjY/6.tlgHhWsbtbAVNR9TSI/:17795:0:99999:7:::"
ROOT_PWD="root:\$6\$klSpdQ1K\$4Gm/7HxehX.YSum4Wf3IDFZ3v5L.clybUpGNGaw9zAh3rqzqB4mWbxvSTFcvhbjY/6.tlgHhWsbtbAVNR9TSI/:17795:0:99999:7:::"

MINIRC_LOGROTATE_DIR="/etc/crob.minirc/"
SYSLOG_MAXSIZE="1000M"
SYSLOG_ROTATE="4"
KERNLOG_MAXSIZE="1000M"
KERNLOG_ROTATE="4"

sectorEnd=`fdisk -l | grep "$DEV_NAME:" | awk -F ' ' '{print $7}'`
sectorSize=`fdisk -l | grep -A 2 "$DEV_NAME:" | grep "Units" | awk -F ' ' '{print $6}'`
if [ $sectorSize -ne 512 ];then
    echo "Failed: sector size is not 512!"
    return 1;
fi
# 536870912 bytes is 512M
sectorRsv=$[536870912/sectorSize+1]
sectorEnd=$[sectorEnd-sectorRsv]

#component main/backup offset
COMPONENTS_MAIN_OFFSET=$[sectorEnd+1]
COMPONENTS_BACKUP_OFFSET=$[COMPONENTS_MAIN_OFFSET+73728]
#0 512k
LPM3_OFFSET=0
LPM3_SIZE=1024
#1M 512k
TEE_OFFSET=2048
TEE_SIZE=1024
#2M 2M
DTB_OFFSET=4096
DTB_SIZE=4096
#32M 32M
IMAGE_OFFSET=8192
IMAGE_SIZE=65536
#component main header
#COMPONENTS_MAIN_HEADER=`${COMPONENTS_MAIN_OFFSET} |awk '{printf("%x\n", $0)}'`
# end

# ************************configure*********************************************
# Description:  configure
# ******************************************************************************
function checkConfig()
{
    source ${MAKECONF} || \
        { echo "[ERROR]${MAKECONF} may has something wrong, please check and repair." && return 1; }
    echo "record ${MAKECONF}" && cat ${MAKECONF}
    return 0
}
# ************************Cleanup*********************************************
# Description:  files cleanup
# ******************************************************************************
function filesClean()
{
    df -h | grep "${TMPDIR_DATE}"
    if [ $? -eq 0 ];then
        umount ${TMPDIR_DATE}
    fi
    rm -rf ${TMPDIR_DATE}
    df -h | grep "${LogPath}squashfs-root/cdtmp"
    if [ $? -eq 0 ];then
        umount ${LogPath}squashfs-root/cdtmp
    fi
    rm -rf ${LogPath}squashfs-root

    rm -rf ${LogPath}filesystem.squashfs
    df -h | grep "${TMPDIR_SD_MOUNT}"
    if [ $? -eq 0 ];then
        umount ${TMPDIR_SD_MOUNT}
    fi
    rm -rf ${TMPDIR_SD_MOUNT}
    df -h | grep "${TMPDIR_SD2_MOUNT}"
    if [ $? -eq 0 ];then
        umount ${TMPDIR_SD2_MOUNT}
    fi
    rm -rf ${TMPDIR_SD2_MOUNT}
    df -h | grep "${TMPDIR_SD3_MOUNT}"
    if [ $? -eq 0 ];then
        umount ${TMPDIR_SD3_MOUNT}
    fi
    rm -rf ${TMPDIR_SD3_MOUNT}

    if [ "$FS_BACKUP_FLAG"x = "on"x ]; then
        df -h | grep "${TMPDIR_SD4_MOUNT}"
        if [ $? -eq 0 ];then
            umount ${TMPDIR_SD4_MOUNT}
        fi
        rm -rf ${TMPDIR_SD4_MOUNT}
    fi

    rm -rf ${LogPath}driver
    return 0
}
#end
# ************************check ip****************************************
# Description:  check ip valid or not
# $1: ip
# ******************************************************************************
function checkIpAddr()
{
    ip_addr=$1
    echo ${ip_addr} | grep "^[0-9]\{1,3\}\.\([0-9]\{1,3\}\.\)\{2\}[0-9]\{1,3\}$" > /dev/null
    if [ $? -ne 0 ]
    then
        return 1
    fi

    for num in `echo ${ip_addr} | sed "s/./ /g"`
    do
        if [ $num -gt 255 ] || [ $num -lt 0 ]
        then
            return 1
        fi
   done
   return 0
}

# **************check network card and usb card ip******************************
# Description:  check network card and usb card ip
# ******************************************************************************
function checkIps()
{
    if [[ ${NETWORK_CARD_DEFAULT_IP}"X" == "X" ]];then
        NETWORK_CARD_DEFAULT_IP="192.168.0.2"
    fi

    checkIpAddr ${NETWORK_CARD_DEFAULT_IP}
    if [ $? -ne 0 ];then
        echo "Failed: Invalid network card ip."
        return 1
    fi
    NETWORK_CARD_GATEWAY=`echo ${NETWORK_CARD_DEFAULT_IP} | sed -r 's/([0-9]+\.[0-9]+\.[0-9]+)\.[0-9]+/\1.1/g'`


    if [[ ${USB_CARD_DEFAULT_IP}"X" == "X" ]];then
        USB_CARD_DEFAULT_IP="192.168.1.2"
    fi

    checkIpAddr ${USB_CARD_DEFAULT_IP}
    if [ $? -ne 0 ];then
        echo "Failed: Invalid usb card ip."
        return 1
    fi
    return 0
}

# **************check driver package and ubuntu iso******************************
# Description:  check driver package and ubuntu iso: file exist, version match
# ******************************************************************************
function checkPackage()
{
    if [[ ${DRIVER_PACKAGE}"X" == "X" ]];then
        echo "[ERROR]Can not find driver package: Ascend310-driver-*.tar.gz."
        return 1
    fi
    if [[ ! -e ${ISO_FILE_DIR}/${ISO_FILE} ]];then
        echo "[ERROR]Can not find iso file."
        return 1
    fi

    if [[ $DRIVER_PACKAGE =~ "18.04" ]];then
        PACKAGE_VERSION="18.04"
	OS_TYPE="Ubuntu"
        ISO_SOURCE_VERSION="bionic main restricted"
        NETWORK_CFG_FILE="/etc/netplan/01-netcfg.yaml"
        NETWORK_CONFIG="
network:
  version: 2
  renderer: networkd
  ethernets:
    eth0:
      dhcp4: no
      addresses: [${NETWORK_CARD_DEFAULT_IP}/24]
      gateway4: ${NETWORK_CARD_GATEWAY}
      nameservers:
            addresses: [255.255.0.0]

    usb0:
      dhcp4: no
      addresses: [${USB_CARD_DEFAULT_IP}/24]
      gateway4: ${NETWORK_CARD_GATEWAY}
"
    elif [[ $DRIVER_PACKAGE =~ "16.04" ]];then
        PACKAGE_VERSION="16.04"
        ISO_SOURCE_VERSION="xenial main restrict"
        NETWORK_CFG_FILE="/etc/network/interfaces"
	OS_TYPE="Ubuntu"
        NETWORK_CONFIG="source /etc/network/interfaces.d/*
auto lo
iface lo inet loopback

auto eth0
iface eth0 inet static
address ${NETWORK_CARD_DEFAULT_IP}
netmask 255.255.255.0
gateway ${NETWORK_CARD_GATEWAY}

auto usb0
iface usb0 inet static
address ${USB_CARD_DEFAULT_IP}
netmask 255.255.255.0
"
    elif [[ $DRIVER_PACKAGE =~ "2.9" ]];then
        PACKAGE_VERSION="V2.0SP9"
        ISO_SOURCE_VERSION=""
	OS_TYPE="EulerOS"
        NETWORK_CFG_FILE="/etc/sysconfig/network-scripts/ifcfg-eth0"
        NETWORK_CONFIG="
TYPE=Ethernet
BOOTPROTO=static
DEFROUTE=yes
NAME=eth0
DEVICE=eth0
ONBOOT=yes
IPADDR=${NETWORK_CARD_DEFAULT_IP}
NETMASK=255.255.255.0
GATEWAY=${NETWORK_CARD_GATEWAY}
"
    else
        echo "unknown driver package version!!!"
        return 1
    fi

    if [[ $ISO_FILE =~ $PACKAGE_VERSION ]];then
        echo "[INFO]Start install ${OS_TYPE}-$PACKAGE_VERSION"
    else
        echo "[ERROR]Driver and ${OS_TYPE} iso do not match, please use ${OS_TYPE}$PACKAGE_VERSION"
        return 1
    fi

    return 0
}

# ************************umount SD Card****************************************
# Description:  check sd card mount, if mounted, umount it
# ******************************************************************************
function checkSDCard()
{
    paths=`df -h | grep "$DEV_NAME" | awk -F ' ' '{print $6}'`
    for path in $paths
    do
        echo "umount $path"
        umount $path
        if [ $? -ne 0 ];then
            echo "Failed: umount $path failed!"
            return 1
        fi
    done
    return 0
}
#end

# ************************Extract ubuntufs from iso*****************************
# Description:  mount iso file , extract root filesystem from squashfs, after
# execute function it will create squashfs-root/ in "./"
# ******************************************************************************
function ubuntufsExtract()
{
    mkdir ${TMPDIR_DATE}
    mount -o loop ${ISO_FILE_DIR}/${ISO_FILE} ${TMPDIR_DATE}

    if [[ $ISO_FILE =~ "ubuntu" ]];then
        cp ${TMPDIR_DATE}/install/filesystem.squashfs ${LogPath}
        if [[ $? -ne 0 ]];then
            echo "Failed: Copy 'filesystem.squashfs' fail!"
            return 1;
        fi

        cd ${LogPath}
        unsquashfs filesystem.squashfs

        if [[ $? -ne 0 ]];then
            echo "Failed: Unsquashfs 'filesystem.squashfs' fail!"
            return 1;
        fi
    elif [[ $ISO_FILE =~ "EulerOS" ]];then
        cd ${LogPath}
        mkdir squashfs-root
        cd squashfs-root
	zcat ${ISO_FILE_DIR}/initrd | cpio -divm

        if [[ $? -ne 0 ]];then
            echo "Failed: cpio file' fail!"
            return 1;
        fi
        cd -
    else
        echo "Failed: iso file not match!"
        return 1;
    fi
    #Return to the bin directory
    cd ${ScriptPath}
    return 0
}
# end


# *****************configure syslog and kernlog**************************************
# Description:  configure syslog and kernlog
# ******************************************************************************
function configure_syslog_and_kernlog()
{
    if [ ! -d ${LogPath}squashfs-root/${MINIRC_LOGROTATE_DIR} ];then
        mkdir -p ${LogPath}squashfs-root/${MINIRC_LOGROTATE_DIR}
    fi
    
    echo "" > ${LogPath}squashfs-root/${MINIRC_LOGROTATE_DIR}minirc_logrotate
    echo "" > ${LogPath}squashfs-root/${MINIRC_LOGROTATE_DIR}minirc_logrotate.conf
    
    cat > ${LogPath}squashfs-root/${MINIRC_LOGROTATE_DIR}minirc_logrotate << EOF
#!/bin/bash

#Clean non existent log file entries from status file
cd /var/lib/logrotate
test -e status || touch status
head -1 status > status.clean
sed 's/"//g' status | while read logfile date
do
    [ -e "\${logfile}" ] && echo "\"\${logfile}\" \${date}"
done >> status.clean

test -x /usr/sbin/logrotate || exit 0
/usr/sbin/logrotate ${MINIRC_LOGROTATE_DIR}minirc_logrotate.conf
EOF

    cat > ${LogPath}squashfs-root/${MINIRC_LOGROTATE_DIR}minirc_logrotate.conf << EOF
# see "main logrotate" for details

# use the syslog group by default, since this is the owing group
# of /var/log/syslog.
su root syslog

# create new (empty) log files after rotating old ones
create
/var/log/syslog
{
        rotate ${SYSLOG_ROTATE}
        weekly
        maxsize ${SYSLOG_MAXSIZE}
        missingok
        notifempty
        compress
        postrotate
                invoke-rc.d rsyslog rotate > /dev/null
        endscript
}
/var/log/kern.log
{
        rotate ${SYSLOG_ROTATE}
        weekly
        maxsize ${SYSLOG_MAXSIZE}
        missingok
        notifempty
        compress
}
EOF
    chmod 755 ${LogPath}squashfs-root/${MINIRC_LOGROTATE_DIR}minirc_logrotate

    echo "*/30 *   * * *   root     cd / && run-parts --report ${MINIRC_LOGROTATE_DIR}" >> ${LogPath}squashfs-root/etc/crontab
    
    if [ -f ${LogPath}squashfs-root/etc/rsyslog.d/50-default.conf ];then
        sed -i 's/*.*;auth,authpriv.none/*.*;auth,authpriv,kern.none/g' ${LogPath}squashfs-root/etc/rsyslog.d/50-default.conf
    fi
    echo 'LogLevel=emerg' >> ${LogPath}squashfs-root/etc/systemd/system.conf
    echo 'MaxLevelStore=emerg' >> ${LogPath}squashfs-root/etc/systemd/journald.conf
    echo 'MaxLevelSyslog=emerg' >> ${LogPath}squashfs-root/etc/systemd/journald.conf
    echo 'MaxLevelKMsg=emerg' >> ${LogPath}squashfs-root/etc/systemd/journald.conf
    echo 'MaxLevelConsole=emerg' >> ${LogPath}squashfs-root/etc/systemd/journald.conf
    echo 'MaxLevelWall=emerg' >> ${LogPath}squashfs-root/etc/systemd/journald.conf
}


# ************************Configure ubuntu**************************************
# Description:  install ssh, configure user/ip and so on
# ******************************************************************************
function configUbuntu()
{
    # 1. configure image sources
    mkdir -p ${LogPath}squashfs-root/cdtmp
    mount -o bind ${TMPDIR_DATE} ${LogPath}squashfs-root/cdtmp
	
    echo "
#!/bin/bash
DRIVER_PACKAGE=\$1
username=\$2
password=\$3
root_pwd=\$4

# 1. apt install deb
mv /etc/apt/sources.list /etc/apt/sources.list.bak
touch /etc/apt/sources.list
echo \"deb file:/cdtmp ${ISO_SOURCE_VERSION}\" > /etc/apt/sources.list

locale-gen zh_CN.UTF-8 en_US.UTF-8 en_HK.UTF-8
apt-get update
echo \"make_sd_process: 5%\"
apt-get install openssh-server -y
apt-get install tar -y
apt-get install unzip -y
apt-get install vim -y
echo \"make_sd_process: 10%\"
apt-get install gcc -y
apt-get install zlib -y
apt-get install python2.7 -y
apt-get install python3 -y
apt-get install curl -y
apt-get install pciutils -y
apt-get install strace -y
apt-get install nfs-common -y
apt-get install sysstat -y
apt-get install libelf1 -y
apt-get install libpython2.7 -y
apt-get install libnuma1 -y
echo \"make_sd_process: 20%\"
apt-get install dmidecode -y
apt-get install rsync -y
apt-get install net-tools -y
echo \"make_sd_process: 25%\"

mv /etc/apt/sources.list.bak /etc/apt/sources.list

# 2. set username
useradd -m \${username} -d /home/\${username} -s /bin/bash
sed -i \"/^\${username}:/c\\\\\${password}\" /etc/shadow
sed -i \"/^root:/c\\\\\${root_pwd}\" /etc/shadow

# 3. config host
echo 'davinci-mini' > /etc/hostname
echo '127.0.0.1        localhost' > /etc/hosts
echo '127.0.1.1        davinci-mini' >> /etc/hosts

# 4. config ip
echo \"$NETWORK_CONFIG\" > $NETWORK_CFG_FILE

# 5. auto-run minirc_cp.sh and minirc_sys_init.sh when start ubuntu
echo \"#!/bin/sh -e
#
# rc.local
#
# This script is executed at the end of each multiuser runlevel.
# Make sure that the script will \"exit 0\" on success or any other
# value on error.
#
# In order to enable or disable this script just change the execution
# bits.
#
# By default this script does nothing.
cd /var/


/bin/bash /var/minirc_boot.sh /opt/mini/${DRIVER_PACKAGE}

if [ -e /var/minirc_hook.sh ];then
     /bin/bash /var/minirc_hook.sh >>/var/minirc_hook.log
fi

exit 0
\" > /etc/rc.local


chmod 755 /etc/rc.local
echo \"RuntimeMaxUse=50M\" >> /etc/systemd/journald.conf
echo \"SystemMaxUse=50M\" >> /etc/systemd/journald.conf

echo \"export LD_LIBRARY_PATH=/home/HwHiAiUser/Ascend/acllib/lib64\" >> /home/HwHiAiUser/.bashrc

exit
# end" > ${LogPath}squashfs-root/chroot_install.sh

    chmod 750 ${LogPath}squashfs-root/chroot_install.sh
    # 2. add user and install software
    # execute in ./chroot_install.sh

    chroot ${LogPath}squashfs-root /bin/bash -c "./chroot_install.sh ${DRIVER_PACKAGE} ${USER_NAME} '"${USER_PWD}"' '"${ROOT_PWD}"'"

    if [[ $? -ne 0 ]];then
        echo "Failed: qemu is broken or the version of qemu is not compatible!"
        return 1;
    fi

    #configure syslog and kern log
    configure_syslog_and_kernlog

    umount ${LogPath}squashfs-root/cdtmp
    rm -rf ${LogPath}squashfs-root/cdtmp
    rm ${LogPath}squashfs-root/chroot_install.sh
    return 0
}

# end
# ************************Configure euler**************************************
# Description:  install ssh, configure user/ip and so on
# ******************************************************************************
function configEuler()
{
    # 1. configure image sources
    mkdir -p ${LogPath}squashfs-root/cdtmp
    mount -o bind /dev ${LogPath}squashfs-root/dev
    mount -o bind ${TMPDIR_DATE} ${LogPath}squashfs-root/cdtmp

    echo "
#!/bin/bash
DRIVER_PACKAGE=\$1
username=\$2
password=\$3
root_pwd=\$4

# 1. yum install rpm
#mv /etc/yum.repos.d/euler_local.repo /etc/yum.repos.d/euler_local.repo.back
touch /etc/yum.repos.d/euler_local.repo
echo \"
[euler-local]
name=euler2.9
baseurl=file:///cdtmp
enable=1
gpgcheck=0\" > /etc/yum.repos.d/euler_local.repo

#locale-gen zh_CN.UTF-8 en_US.UTF-8 en_HK.UTF-8
yum update
echo \"make_sd_process: 5%\"
yum install openssh-server -y
yum install tar -y
yum install unzip -y
yum install vim -y
echo \"make_sd_process: 10%\"
yum install gcc -y
yum install zlib -y
yum install python3 -y
yum install pciutils -y
yum install strace -y
yum install sysstat -y
echo \"make_sd_process: 20%\"
yum install dmidecode -y
yum install rsync -y
yum install net-tools -y
echo \"make_sd_process: 25%\"

#mv /etc/yum.repos.d/euler_local.repo.back /etc/yum.repos.d/euler_local.repo

# 2. set username
useradd -m \${username} -d /home/\${username} -s /bin/bash
sed -i \"/^\${username}:/c\\\\\${password}\" /etc/shadow
sed -i \"/^root:/c\\\\\${root_pwd}\" /etc/shadow

# 3. config host
echo 'davinci-mini' > /etc/hostname
echo '127.0.0.1        localhost' > /etc/hosts
echo '127.0.1.1        davinci-mini' >> /etc/hosts

# 4. config ip
echo \"$NETWORK_CONFIG\" > $NETWORK_CFG_FILE

# 5. auto-run minirc_cp.sh and minirc_sys_init.sh when start system
echo \"#!/bin/sh -e
# rc.local
cd /var/

/bin/bash /var/minirc_boot.sh /opt/mini/${DRIVER_PACKAGE}

if [ -e /var/minirc_hook.sh ];then
     /bin/bash /var/minirc_hook.sh >>/var/minirc_hook.log
fi

exit 0
\" > /etc/rc.local
chmod 755 /etc/rc.local

# 6. enable su command default
sed -i '/pam_wheel/ s/^/#/' /etc/pam.d/su

echo \"RuntimeMaxUse=50M\" >> /etc/systemd/journald.conf
echo \"SystemMaxUse=50M\" >> /etc/systemd/journald.conf

echo \"export LD_LIBRARY_PATH=/home/HwHiAiUser/Ascend/acllib/lib64\" >> /home/HwHiAiUser/.bashrc

exit
# end" > ${LogPath}squashfs-root/chroot_install.sh

    chmod 750 ${LogPath}squashfs-root/chroot_install.sh
    # 2. add user and install software
    # execute in ./chroot_install.sh

    cat ${LogPath}squashfs-root/chroot_install.sh

    chroot ${LogPath}squashfs-root /bin/bash -c "./chroot_install.sh ${DRIVER_PACKAGE} ${USER_NAME} '"${USER_PWD}"' '"${ROOT_PWD}"'"

    if [[ $? -ne 0 ]];then
        umount ${LogPath}squashfs-root/dev
        echo "Failed: qemu is broken or the version of qemu is not compatible!"
        return 1;
    fi

    #configure syslog and kern log
    configure_syslog_and_kernlog

    umount ${LogPath}squashfs-root/cdtmp
    umount ${LogPath}squashfs-root/dev
    rm -rf ${LogPath}squashfs-root/cdtmp
    rm -rf ${LogPath}squashfs-root/dev
    rm ${LogPath}squashfs-root/chroot_install.sh
    return 0
}

function configFilesystem()
{
    if [[ $ISO_FILE =~ "ubuntu" ]];then
        configUbuntu
        if [ $? -ne 0 ];then
            echo "Failed: config ubuntu fail!"
            return 1
        fi

        return 0
    elif [[ $ISO_FILE =~ "EulerOS" ]];then
        configEuler
        if [ $? -ne 0 ];then
            echo "Failed: config euleros fail!"
            return 1
        fi

        return 0
    else
        echo "Failed: unsupport os!"
        return 1
    fi
}

# ************************Format SDcard*****************************************
# Description:  format to ext3 filesystem and three partition
# ******************************************************************************
function formatSDcardFsBackup()
{
    if [[ $(fdisk -l 2>/dev/null | grep "^${DEV_NAME}" | wc -l) -gt 1 ]];then
    for i in $(fdisk -l 2>/dev/null | grep "^${DEV_NAME}" | awk -F ' ' '{print $1}'); do
            echo "d

        w" | fdisk ${DEV_NAME}
    done
    else
    echo "d

    w" | fdisk ${DEV_NAME}
    fi
    umount ${DEV_NAME} 2>/dev/null

    sectorOffset_1M=$[1024*1024/sectorSize]
    sectorOffset1=$sectorOffset_1M
    # End1: rootfs_part_size + 1M - 1
    sectorEnd1=$[sectorOffset1+ROOT_PART_SIZE*sectorOffset_1M-1]
    # Offset2: rootfs_part_size + 1M
    sectorOffset2=$[sectorEnd1+1]
    # End2: rootfs_part_size + 1M - 1
    sectorEnd2=$[sectorOffset2+LOG_PART_SIZE*sectorOffset_1M-1]
    # Offset3: sectorEnd2 + 1
    sectorOffset3=$[sectorEnd2+1]
    # End3: sectorEnd - p4 size
    sectorEnd3=$[sectorEnd-ROOT_PART_SIZE*sectorOffset_1M]
    # Offset4: End3 + 1
    sectorOffset4=$[sectorEnd3+1]

    #verifying the capacity
    curSector=$[sectorEnd2+ROOT_PART_SIZE*sectorOffset_1M]
    if [ $curSector -ge $sectorEnd ]; then
        echo "Failed: SD/eMMC capacity is less than user Configuration, please reconfigure and make later"
        return 1
    fi

    echo "n
p
1
$sectorOffset1
$sectorEnd1
n
p
2
$sectorOffset2
$sectorEnd2
n
p
3
$sectorOffset3
$sectorEnd3
n
p
$sectorOffset4
$sectorEnd
w
" | fdisk ${DEV_NAME}

    partprobe

    fdisk -l

    sleep 5

    checkSDCard
    if [ $? -ne 0 ];then
        return 1
    fi
    echo "y
    " | mkfs.ext3 -L ubuntu_fs ${DEV_NAME}$p1
    if [[ $? -ne 0 ]];then
        echo "Failed: Format SDcard1 failed!"
        return 1;
    fi

    echo "make_sd_process: 30%"
    checkSDCard
    if [ $? -ne 0 ];then
        return 1
    fi
    echo "y
    " | mkfs.ext3 -L ubuntu_fs ${DEV_NAME}$p2
    if [[ $? -ne 0 ]];then
        echo "Failed: Format SDcard2 failed!"
        return 1;
    fi

    echo "make_sd_process: 35%"
    checkSDCard
    if [ $? -ne 0 ];then
        return 1
    fi
    echo "y
    " | mkfs.ext3 -L ubuntu_fs ${DEV_NAME}$p3
    if [[ $? -ne 0 ]];then
        echo "Failed: Format SDcard3 failed!"
        return 1;
    fi
    echo "make_sd_process: 45%"
    echo "y
    " | mkfs.ext3 -L ubuntu_fs ${DEV_NAME}$p4
    if [[ $? -ne 0 ]];then
        echo "Failed: Format SDcard4 failed!"
        return 1;
    fi
    return 0
}

# ************************Format SDcard*****************************************
# Description:  format to ext3 filesystem and three partition
# ******************************************************************************
function formatSDcard()
{
    if [[ $(fdisk -l 2>/dev/null | grep "^${DEV_NAME}" | wc -l) -gt 1 ]];then
	for i in $(fdisk -l 2>/dev/null | grep "^${DEV_NAME}" | awk -F ' ' '{print $1}'); do
            echo "d

	    w" | fdisk ${DEV_NAME}
	done
    else
	echo "d

	w" | fdisk ${DEV_NAME}
    fi
    umount ${DEV_NAME} 2>/dev/null

    sectorOffset_1M=$[1024*1024/sectorSize]
    sectorOffset1=$sectorOffset_1M
    # End1: rootfs_part_size + 1M - 1
    sectorEnd1=$[sectorOffset1+ROOT_PART_SIZE*sectorOffset_1M-1]
    # Offset2: rootfs_part_size + 1M
    sectorOffset2=$[sectorEnd1+1]
    # End2: rootfs_part_size + 1M - 1
    sectorEnd2=$[sectorOffset2+LOG_PART_SIZE*sectorOffset_1M-1]
    # Offset3: sectorEnd2 + 1
    sectorOffset3=$[sectorEnd2+1]

    #verifying the capacity
    curSector=$sectorEnd2
    if [ $curSector -ge $sectorEnd ]; then
        echo "Failed: SD/eMMC capacity is less than user Configuration, please reconfigure and make later"
        return 1
    fi

    echo "n
p
1
$sectorOffset1
$sectorEnd1
n
p
2
$sectorOffset2
$sectorEnd2
n
p
3
$sectorOffset3
$sectorEnd
w
" | fdisk ${DEV_NAME}

    partprobe

    fdisk -l

    sleep 5

    checkSDCard
    if [ $? -ne 0 ];then
        return 1
    fi
    echo "y
    " | mkfs.ext3 -L ubuntu_fs ${DEV_NAME}$p1
    if [[ $? -ne 0 ]];then
        echo "Failed: Format SDcard1 failed!"
        return 1;
    fi

    echo "make_sd_process: 30%"
    checkSDCard
    if [ $? -ne 0 ];then
        return 1
    fi
    echo "y
    " | mkfs.ext3 -L ubuntu_fs ${DEV_NAME}$p2
    if [[ $? -ne 0 ]];then
        echo "Failed: Format SDcard2 failed!"
        return 1;
    fi

    echo "make_sd_process: 35%"
    checkSDCard
    if [ $? -ne 0 ];then
        return 1
    fi
    echo "y
    " | mkfs.ext3 -L ubuntu_fs ${DEV_NAME}$p3
    if [[ $? -ne 0 ]];then
        echo "Failed: Format SDcard3 failed!"
        return 1;
    fi
    echo "make_sd_process: 45%"
    return 0
}
#end

# ************************Copy files to SD**************************************
# Description:  copy rar and root filesystem to SDcard
# ******************************************************************************
function preInstallDriver()
{
    echo "start pre install driver"
	mkdir -p ${LogPath}squashfs-root/opt/mini
    chmod 755 ${LogPath}squashfs-root/opt/mini
	
    # 1. copy third party file
    tar zxf ${ISO_FILE_DIR}/${DRIVER_PACKAGE} -C ${LogPath} driver/scripts/minirc_install_phase1.sh 
    cp ${LogPath}driver/scripts/minirc_install_phase1.sh ${LogPath}squashfs-root/opt/mini/
    if [[ $? -ne 0 ]];then
        echo "Failed: Copy minirc_install_phase1.sh to filesystem failed!"
        return 1
    fi
    chmod +x ${LogPath}/driver/scripts/minirc_install_phase1.sh

    echo "make_sd_process: 75%"

    tar -zxf ${ISO_FILE_DIR}/${DRIVER_PACKAGE} -C ${LogPath} driver/scripts/minirc_boot.sh
    cp ${LogPath}driver/scripts/minirc_boot.sh ${LogPath}squashfs-root/var/
    if [[ $? -ne 0 ]];then
        echo "Failed: Copy minirc_boot.sh to filesystem failed!"
        return 1
    fi

#    tar -zxf ${ISO_FILE_DIR}/${DRIVER_PACKAGE} -C ${LogPath} driver/extend_rootfs/perf
#    cp ${LogPath}driver/extend_rootfs/perf ${LogPath}squashfs-root/usr/bin/perf
#    if [[ $? -ne 0 ]];then
#        echo "Failed: Copy perf.sh to filesystem failed!"
#        return 1
#    fi
#    chmod +x ${LogPath}squashfs-root/usr/bin/perf

    echo "make_sd_process: 80%"
    # 2. copy root filesystem
    if [[ ${arch} =~ "x86" ]];then
        rm ${LogPath}squashfs-root/usr/bin/qemu-aarch64-static
    fi

    #install $DRIVER_PACKAGE
    mkdir -p ${LogPath}mini_pkg_install/opt/mini
    cp ${ISO_FILE_DIR}/${DRIVER_PACKAGE}  ${LogPath}mini_pkg_install/opt/mini/
    chmod +x ${LogPath}squashfs-root/opt/mini/minirc_install_phase1.sh
    ${LogPath}driver/scripts/minirc_install_phase1.sh ${LogPath}mini_pkg_install
    res=$(echo $?)
    if [[ ${res} != "0" ]];then
        echo "Install ${DRIVER_PACKAGE} fail, error code:${res}"
        echo "Failed: Install ${DRIVER_PACKAGE} failed!"
        return 1
    fi
    
}

function copyFilesToSDcard()
{
    cp -a ${LogPath}squashfs-root/* ${TMPDIR_SD_MOUNT}
    if [[ $? -ne 0 ]];then
        echo "Failed: Copy root filesystem to SDcard failed!"
        return 1
    fi

    if [ "$FS_BACKUP_FLAG"x = "on"x ]; then
        cp -a ${LogPath}squashfs-root/* ${TMPDIR_SD4_MOUNT}
        if [[ $? -ne 0 ]];then
            echo "Failed: Copy root filesystem to backup SDcard failed!"
            return 1
        fi
    fi

    cp -rf ${TMPDIR_SD_MOUNT}/home/* ${TMPDIR_SD3_MOUNT}/
    #rm -rf ${TMPDIR_SD_MOUNT}/home/*

    cp -rf ${TMPDIR_SD_MOUNT}/var/log/* ${TMPDIR_SD2_MOUNT}/
    #rm -rf ${TMPDIR_SD_MOUNT}/var/log/*
    echo "make_sd_process: 90%"
    return 0
}

function preInstallHook()
{
    if [ -e "${ScriptPath}/minirc_install_hook.sh" ];then
        bash ${ScriptPath}/minirc_install_hook.sh "${LogPath}squashfs-root/"
        if [ $? -ne 0 ];then
            echo "Excute minirc_install_hook.sh failed"
            return 1
        else
            echo "Excute minirc_install_hook.sh success"
        fi
    fi
    return 0
}

function preInstallMinircPackage()
{
    preInstallDriver
    if [ $? -ne 0 ];then
        echo "Pre install driver package failed"
        return 1
    fi

    preInstallHook
    if [ $? -ne 0 ];then
        return 1
    fi

    rm -rf ${LogPath}mini_pkg_install/opt
    cp -rf ${LogPath}mini_pkg_install/* ${LogPath}squashfs-root/
    if [[ $? -ne 0 ]];then
        echo "Failed: Copy mini_pkg_install to filesystem failed!"
        return 1
    fi
    echo "pre install drvier finished"
    echo "make_sd_process: 85%"
    rm -rf ${LogPath}mini_pkg_install

    copyFilesToSDcard
    if [ $? -ne 0 ];then
        echo "Copy file to sdcard failed"
        return 1
    fi
}
# end

# ************************fillSectors****************************
# input:
# parameters1: output image
# ***************************************************************
function fillSectors()
{
    local file_size=`wc -c < $1`
    local seek_cnt=$[${file_size}/4]
    local word_cnt=$[128-${seek_cnt}]

    dd if=/dev/zero of=$1 count=${word_cnt} bs=4 seek=${seek_cnt}
}

# ************************writePartitionHeader**************************************
# Description:  write partirion header
# ******************************************************************************
function writePartitionHeader()
{
    #sector 512ñ
    secStart=16
    MAIN_HEADER=$(printf "%#x" $COMPONENTS_MAIN_OFFSET)
    BACK_HEADER=$(printf "%#x" $COMPONENTS_BACKUP_OFFSET)

    MAIN_A=$(printf "%x" $(( ($MAIN_HEADER & 0xFF000000) >> 24 )))
    MAIN_B=$(printf "%x" $(( ($MAIN_HEADER & 0x00FF0000) >> 16 )))
    MAIN_C=$(printf "%x" $(( ($MAIN_HEADER & 0x0000FF00) >> 8)))
    MAIN_D=$(printf "%x" $(( $MAIN_HEADER & 0x000000FF )))

    BACKUP_A=$(printf "%x" $(( ($BACK_HEADER & 0xFF000000) >> 24 )))
    BACKUP_B=$(printf "%x" $(( ($BACK_HEADER & 0x00FF0000) >> 16 )))
    BACKUP_C=$(printf "%x" $(( ($BACK_HEADER & 0x0000FF00) >> 8)))
    BACKUP_D=$(printf "%x" $(( $BACK_HEADER & 0x000000FF )))

    if [ "$FS_BACKUP_FLAG"x = "on"x ]; then
        echo -e -n "\x55\xAA\x55\xAA\x44\xBB\x44\xBB" > magic
    else
        echo -e -n "\x55\xAA\x55\xAA" > magic
    fi
    echo -e -n "\x$MAIN_D\x$MAIN_C\x$MAIN_B\x$MAIN_A" > components_main_base
    echo 0000 0000 0000 0000 0000 0000\
        0004 0000 0000 0000 0008 0000 0000 0000\
        0004 0000 0000 0000 0010 0000 0000 0000\
        0010 0000 0000 0000 0020 0000 0000 0000\
        0000 0100 0000 0000 0000 0000 0000 0000\
        0000 0000 0000 0000 0000 0000 0000 0000 | xxd -r -ps >> components_main_base

    echo -e -n "\x$BACKUP_D\x$BACKUP_C\x$BACKUP_B\x$BACKUP_A" > components_backup_base
    echo 0000 0000 0000 0000 0000 0000\
        0004 0000 0000 0000 0008 0000 0000 0000\
        0004 0000 0000 0000 0010 0000 0000 0000\
        0010 0000 0000 0000 0020 0000 0000 0000\
        0000 0100 0000 0000 0000 0000 0000 0000\
        0000 0000 0000 0000 0000 0000 0000 0000 | xxd -r -ps >> components_backup_base

    fillSectors magic
    fillSectors components_main_base
    fillSectors components_backup_base

    dd if=magic of=${DEV_NAME} count=1 seek=$[secStart] bs=$sectorSize
    dd if=magic of=${DEV_NAME} count=1 seek=$[secStart+1] bs=$sectorSize
    dd if=components_main_base of=${DEV_NAME} count=1 seek=$[secStart+2] bs=$sectorSize
    dd if=components_backup_base of=${DEV_NAME} count=1 seek=$[secStart+3] bs=$sectorSize

    rm -rf magic
    rm -rf components_main_base
    rm -rf components_backup_base
}


# ************************writeComponents**************************************
# Description:  write components main/backup
# ******************************************************************************
function writeComponents()
{
    FWM_DIR="${LogPath}squashfs-root/fw/"
    OF_DIR=$1

    if [[ -d "${FWM_DIR}" ]];then
        echo "fw dir exist"
    else
        echo "failed: fw dir no exist"
        return 1
    fi

    dd if=${FWM_DIR}lpm3.img of=${DEV_NAME} count=$LPM3_SIZE seek=$[OF_DIR+LPM3_OFFSET] bs=$sectorSize
    if [ $? -ne 0 ];then
        echo "failed: $OF_DIR lpm3"
        return 1
    fi
    dd if=${FWM_DIR}tee.bin of=${DEV_NAME} count=$TEE_SIZE seek=$[OF_DIR+TEE_OFFSET] bs=$sectorSize
    if [ $? -ne 0 ];then
        echo "failed: $OF_DIR tee"
        return 1
    fi
    dd if=${FWM_DIR}dt.img of=${DEV_NAME} count=$DTB_SIZE seek=$[OF_DIR+DTB_OFFSET] bs=$sectorSize
    if [ $? -ne 0 ];then
        echo "failed: $OF_DIR dt"
        return 1
    fi    
    dd if=${FWM_DIR}Image of=${DEV_NAME} count=$IMAGE_SIZE seek=$[OF_DIR+IMAGE_OFFSET] bs=$sectorSize
    if [ $? -ne 0 ];then
        echo "failed: $OF_DIR Image"
        return 1
    fi
}

# ########################Begin Executing######################################
# ************************Check args*******************************************
function main()
{
    echo "make_sd_process: 2%"
    if [[ $# -lt 6 ]];then
        echo "Failed: Number of parameter illegal! Usage: $0 <dev fullname> <img path> <iso fullname> <net ip> <usb net ip> <result filename>"
        return 1;
    fi
    if [ X${MAKE_OS_RESULT} = "X" ];then
        echo "Failed: Result file name is NULL."
        return 1;
    fi

    # ********************** check configuration **************************
    checkConfig || return 1

    # ***************check network and usb card ip**********************************
    checkIps
    if [ $? -ne 0 ];then
        return 1
    fi
    # ***************check driver package and ubuntu iso**********************************
    checkPackage
    if [ $? -ne 0 ]; then
        return 1
    fi
    # ************************umount dev_name***************************************
    checkSDCard
    if [ $? -ne 0 ];then
        return 1
    fi

    # ************************Extract ubuntufs**************************************
    # output:squashfs-root/
    ubuntufsExtract
    if [ $? -ne 0 ];then
        return 1
    fi
    # end

    # ************************Check architecture************************************
    arch=$(uname -m)
    if [[ ${arch} =~ "x86" ]];then
         cp /usr/bin/qemu-aarch64-static ${LogPath}squashfs-root/usr/bin/
         if [ $? -ne 0 ];then
             echo "Failed: qemu-user-static or binfmt-support not found!"
             return 1;
         fi
         chmod 755 ${LogPath}squashfs-root/usr/bin/qemu-aarch64-static
    fi
    # end

    # ************************Configure ubuntu**************************************
    echo "Process: 1/4(Configure filesystem)"
    configFilesystem
    if [ $? -ne 0 ];then
        return 1
    fi
    # end

    # ************************Format SDcard*****************************************
    echo "Process: 2/4(Format SDcard)"
    if [ "$FS_BACKUP_FLAG"x = "on"x ]; then
        formatSDcardFsBackup || return 1
    else
        formatSDcard || return 1
    fi

    # ************************Copy files to SD**************************************
    if [[ -d "${TMPDIR_SD_MOUNT}" ]];then
        umount ${TMPDIR_SD_MOUNT} 2>/dev/null
        rm -rf ${TMPDIR_SD_MOUNT}
    fi
    mkdir ${TMPDIR_SD_MOUNT}
    mount ${DEV_NAME}$p1 ${TMPDIR_SD_MOUNT} 2>/dev/null

    if [ "$FS_BACKUP_FLAG"x = "on"x ]; then
        if [[ -d "${TMPDIR_SD4_MOUNT}" ]];then
            umount ${TMPDIR_SD4_MOUNT} 2>/dev/null
            rm -rf ${TMPDIR_SD4_MOUNT}
        fi
        mkdir ${TMPDIR_SD4_MOUNT}
        mount ${DEV_NAME}$p4 ${TMPDIR_SD4_MOUNT} 2>/dev/null
    fi

    if [[ -d "${TMPDIR_SD2_MOUNT}" ]];then
        umount ${TMPDIR_SD2_MOUNT} 2>/dev/null
        rm -rf ${TMPDIR_SD2_MOUNT}
    fi
    mkdir ${TMPDIR_SD2_MOUNT}
    mount ${DEV_NAME}$p2 ${TMPDIR_SD2_MOUNT} 2>/dev/null
    echo "make_sd_process: 50%"

    if [[ -d "${TMPDIR_SD3_MOUNT}" ]];then
        umount ${TMPDIR_SD3_MOUNT} 2>/dev/null
        rm -rf ${TMPDIR_SD3_MOUNT}
    fi
    mkdir ${TMPDIR_SD3_MOUNT}
    mount ${DEV_NAME}$p3 ${TMPDIR_SD3_MOUNT} 2>/dev/null
    echo "make_sd_process: 55%"

    echo "Process: 3/4(Pre install each run package and copy filesystem to SDcard)"
    preInstallMinircPackage    
    if [ $? -ne 0 ];then
        return 1
    fi

    # end

    # ************************write Components************************************** 
    echo "Process: 4/4(Write main/backup)"
    writeComponents COMPONENTS_MAIN_OFFSET
    if [ $? -ne 0 ];then
        echo "Failed: writeComponents main"
        return 1
    fi
    echo "writeComponents main Succ"
    # end

    writeComponents COMPONENTS_BACKUP_OFFSET
    if [ $? -ne 0 ];then
        echo "Failed: writeComponents backup"
        return 1s
    fi
    echo "writeComponents backup Succ"
    # end

    # ************************write Partition Header********************************
    writePartitionHeader
    if [ $? -ne 0 ];then
        echo "Failed: writePartitionHeader"
        return 1
    fi
    echo "writePartitionHeader Succ"
    #end

    umount ${TMPDIR_SD_MOUNT} 2>/dev/null
    if [[ $? -ne 0 ]];then
        echo "Failed: Umount ${TMPDIR_SD_MOUNT} to SDcard failed!"
        return 1
    fi
 
    umount ${TMPDIR_SD2_MOUNT} 2>/dev/null
    if [[ $? -ne 0 ]];then
        echo "Failed: Umount ${TMPDIR_SD2_MOUNT} to SDcard failed!"
        return 1
    fi

    umount ${TMPDIR_SD3_MOUNT} 2>/dev/null
    if [[ $? -ne 0 ]];then
        echo "Failed: Umount ${TMPDIR_SD3_MOUNT} to SDcard failed!"
        return 1
    fi
    if [ "$FS_BACKUP_FLAG"x = "on"x ]; then
        umount ${TMPDIR_SD4_MOUNT} 2>/dev/null
        if [[ $? -ne 0 ]];then
            echo "Failed: Umount ${TMPDIR_SD4_MOUNT} to SDcard failed!"
            return 1
        fi
    fi
    echo "Finished!"
    return 0
}

main $*
ret=$?
#clean files
echo "make sd car finished ,clean files" 
filesClean

if [[ ret -ne 0 ]];then
    echo "Failed" > ${LogPath}/${MAKE_OS_RESULT}
    exit 1
fi
echo "Success" > ${LogPath}/${MAKE_OS_RESULT}
exit 0
# end
